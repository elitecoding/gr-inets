/* -*- c++ -*- */
/*
 * Copyright 2015 <+YOU OR YOUR COMPANY+>.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include "gain_control_impl.h"

namespace gr {
  namespace inets {

    gain_control::sptr
    gain_control::make(float threshold)
    {
      return gnuradio::get_initial_sptr
        (new gain_control_impl(threshold));
    }

    /*
     * The private constructor
     */
    gain_control_impl::gain_control_impl(float threshold)
      : gr::sync_block("gain_control",
              gr::io_signature::make(1, 1, sizeof(std::complex<float>)),
              gr::io_signature::make(1, 1, sizeof(std::complex<float>))),
        state(STATE_DETECT), gain(1.0f), mag_threshold(threshold)
    {

        std::cout << "State = " << this->state << " thresh = "
             << this->mag_threshold << std::endl;
    }

    /*
     * Our virtual destructor.
     */
    gain_control_impl::~gain_control_impl()
    {
    }

    int
    gain_control_impl::work(int noutput_items,
			  gr_vector_const_void_star &input_items,
			  gr_vector_void_star &output_items)
    {
        const std::complex<float> *in = (const std::complex<float> *) input_items[0];
        std::complex<float> *out = (std::complex<float> *) output_items[0];

        // Do <+signal processing+>
        float curr_mag = 0;
        for(int i = 0; i < noutput_items; i++) {

            curr_mag = std::abs(in[i]);

            if(this->state == STATE_DETECT) {
                if(curr_mag > mag_threshold) {
                    this->state = STATE_VALIDATE_EDGE;
                    this->edge_counter = 1;
                }
            }

            if(this->state == STATE_VALIDATE_EDGE) {
                if(curr_mag > mag_threshold) {
                    this->edge_counter++;
                } else {
                    this->edge_counter = 0;
                    this->state = STATE_DETECT;
                }
                if(this->edge_counter >= 0) {
                    this->state = STATE_DELAY;
                }
            }

            if(this->state == STATE_DELAY) {
                this->edge_counter++;
                if(this->edge_counter >= 1024) {
                    this->state = STATE_PACKET;
                    this->edge_counter = 0;
                }
            }

            if(this->state == STATE_PACKET) {
                float avg = 0.0f;
                if(noutput_items - i > 128) {
                    for(int j = 0; j < 128; j++) {
                        avg += std::abs(in[j + i]);
                    }
                    avg /= 128.0f;
                    this->gain = 1.0 / avg;
                    //std::cout << "New gain = " << this->gain << std::endl;
                    this->state = STATE_WAIT_FOR_END;
                    this->edge_counter = 0;
                } else {
                    //std::cout << "Not enough data. i = " << i << " n = " << noutput_items << std::endl;
                    return i;
                }
            }

            if(state == STATE_WAIT_FOR_END) {
                if(curr_mag <= mag_threshold) {
                    this->edge_counter++;
                } else {
                    this->edge_counter = 0;
                }
                if(this->edge_counter >= 128) {
                    this->state = STATE_DETECT;
                }
            }

            out[i] = in[i] * this->gain;
        }
        // Tell runtime system how many output items we produced.
        return noutput_items;
    }

  } /* namespace inets */
} /* namespace gr */

